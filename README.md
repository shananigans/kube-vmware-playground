# kube-vmware-playground

Playbooks created while learning packer, cloud-init and terraform with vmware.

## Pulling down dependancies for off-line environment

The depdownloader.yml playbook does the following:

* Creates the base ./dependencies directory.
* Downloads a list of iso file sto ./dependencies/isos/
* Downloads a list of generic files to ./dependencies/files/
* Probes for your workstation's container cli(podman or docker)
* Downloads a list of container images with your container cli to ./dependencies/containers.
* Downloads a list of rpms to ./dependencies/rpms for redhat family images.
* (todo). Create and test dpkg download to ./dependencies/dpkgs for debian family images.
```
ansible-galaxy collection install -r ansible-requirements.txt

ansible-playbook depdownloader.yml -e @vars/environment.yml
```

## Building the image

The create_base_image.yml playbook does the following:
* prompts for password if vcenter_password override isn't provided.
* generates ssh keys under ssh_keys/packer{.pub}
* generates firewall rules to allow port 8080 to the vmware network
* generates a packer config under ./packer/packer_build.json
* generates a ./dependencies/kickstarts/ks.cfg kickstart file for the vmware-iso install
* runs packer using the vmware-iso module and creates the template.
* shuts down port 8080 on the firewall
* cleans up all generated files


```
ansible-galaxy collection install -r ansible-requirements.txt

# Full run
ansible-playbook create_base_image.yml -e @vars/environment.yml


# Running packer(tag: create) step manually for troubleshooting
ansible-playbook create_base_image.yml -e @vars/environment.yml --tags prep
sudo /usr/bin/packer build packer/packer_build.json
ansible-playbook create_base_image.yml -e @vars/environment.yml --tags clean
```

## Build out the infrastructure using terraform.

The infrastructure.yml playbook does the following:
* prompts for password if vcenter_password override isn't provided.
* generates ssh keys under ssh_keys/terraform{.pub}
* generates the terraform/<provider/terraform.tfvars file
* creates the './inventory' directory
* runs terraform init/apply to build out the env.
* terraform outputs a './inventory/inventory.ini' file to be used by ansible when installing kubernetes.

```
# To build out the nodes
ansible-playbook infrastructure.yml -e @vars/environment.yml

# To destroy the nodes and do some cleanup
ansible-playbook infrastructure.yml -e @vars/environment.yml -e 'terraform_action=destroy'
```

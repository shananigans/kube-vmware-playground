#=================================================================================
# This file establishes a connection and authenticates with your vSphere provider
#=================================================================================

provider "vsphere" {
  vsphere_server        = var.vsphere_server_ip
  user                  = var.vsphere_user
  password              = var.vsphere_user_password
  allow_unverified_ssl  = var.allow_unverified_ssl
}


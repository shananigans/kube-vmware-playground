# Generate an ansible inventory file for later use
resource "local_file" "AnsibleInventory" {
  content = templatefile("templates/ansible_inventory.ini.tpl",
    {
      master_names          = vsphere_virtual_machine.master-node.*.name
      master_ips            = vsphere_virtual_machine.master-node.*.default_ip_address
      worker_names          = vsphere_virtual_machine.worker-node.*.name
      worker_ips            = vsphere_virtual_machine.worker-node.*.default_ip_address
      username              = var.node_user
      node_ssh_priv_keyfile = var.node_ssh_priv_keyfile
    }
  )
  filename = format("%s/inventory.ini", var.ansible_inventory_dir)
}

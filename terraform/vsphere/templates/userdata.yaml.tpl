#cloud-config
users:
  - name: ${node_user}
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys:
      - ${node_ssh_key}


[all]
%{ for index, hostname in master_names ~}
${hostname} ansible_host=${master_ips[index]} ansible_user=${username} ansible_ssh_private_key_file=${node_ssh_priv_keyfile}
%{ endfor ~}
%{ for index, hostname in worker_names ~}
${hostname} ansible_host=${worker_ips[index]} ansible_user=${username} ansible_ssh_private_key_file=${node_ssh_priv_keyfile}
%{ endfor ~}

[etcd]
%{ for index, hostname in master_names ~}
${hostname}
%{ endfor ~}

[kube-master]
%{ for index, hostname in master_names ~}
${hostname}
%{ endfor ~}

[kube-node]
%{ for index, hostname in worker_names ~}
${hostname}
%{ endfor ~}

[k8s-cluster:children]
kube-master
kube-node

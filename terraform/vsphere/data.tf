#=============================================================================
# This file holds the data used to provision the cluster infra inside vSphere
#=============================================================================

data "vsphere_datacenter" "datacenter" {
  name = var.datacenter
}

data "vsphere_datastore" "datastore" {
  name          = var.datastore
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_compute_cluster" "cluster" {
  name          = var.cluster
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_network" "network" {
  name          = var.network
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_virtual_machine" "cloud-init-template" {
  name          = var.cloud_init_template_image
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

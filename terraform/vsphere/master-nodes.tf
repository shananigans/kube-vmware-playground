#===========================================================
# This file creates Master Node resources inside of vSphere
#===========================================================

resource "vsphere_virtual_machine" "master-node" {
  count            = var.master_count
  name             = format("%s-%s", var.master_name, count.index + 1)
  resource_pool_id = data.vsphere_compute_cluster.cluster.resource_pool_id
  datastore_id     = data.vsphere_datastore.datastore.id
  folder           = var.cluster_folder
  
  num_cpus = var.master_num_cpus
  memory   = var.master_memory
 # wait_for_guest_net_timeout = 0
  guest_id = data.vsphere_virtual_machine.cloud-init-template.guest_id
  
  scsi_type = data.vsphere_virtual_machine.cloud-init-template.scsi_type

 # firmware = "efi"

  network_interface {
    network_id   = data.vsphere_network.network.id
    adapter_type = data.vsphere_virtual_machine.cloud-init-template.network_interface_types[0]
  }

  disk {
    label            = "disk0"
    size             = data.vsphere_virtual_machine.cloud-init-template.disks.0.size
    eagerly_scrub    = data.vsphere_virtual_machine.cloud-init-template.disks.0.eagerly_scrub
    thin_provisioned = data.vsphere_virtual_machine.cloud-init-template.disks.0.thin_provisioned
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.cloud-init-template.id
  }

  extra_config = {
    "guestinfo.metadata"          = base64encode(templatefile("${path.cwd}/templates/metadata.yaml.tpl", {hostname = format("%s-%s", var.master_name, count.index + 1)}))
    "guestinfo.metadata.encoding" = "base64"
    "guestinfo.userdata"          = base64encode(templatefile("${path.cwd}/templates/userdata.yaml.tpl", {node_ssh_key = var.node_ssh_key, node_user = var.node_user}))
    "guestinfo.userdata.encoding" = "base64"
  }
  
}


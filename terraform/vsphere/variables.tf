#+++++++++++++++++++++++++++++++++++++++++++++++++
# All variables are defined in terraform.tfvars  +
#+++++++++++++++++++++++++++++++++++++++++++++++++

#====================================================
# vSphere Connection - provider.tf variable blocks
#====================================================

variable "vsphere_server_ip" {
  description = "This variable sets the sever's ip address for the connection."
  type = string
}

variable "vsphere_user" {
  description = "This variable stores your vSphere username."
  type = string
}

variable "vsphere_user_password" {
  description = "This variable stores your vSphere password."
  type = string
}

variable "allow_unverified_ssl" {
  description = "This variable allows unverified ssl for the connection."
  type = string
}

variable "cluster_folder" {
  description = "This variable provisions the master and worker nodes in the specified folder."
  type = string
}

#===================================================
# vSphere Data - data.tf variable blocks
#===================================================

variable "datacenter" {
  description = "This variable stores the vSphere datacenter."
}

variable "datastore" {
  description = "This variable stores the vSphere datastore."
}

variable "cluster" {
  description = "This variabe stores the vSpere cluster / root resource pool."
}

variable "network" {
  description = "This variable stores the vSphere vm network."
}

#===================================================
# vSphere Data - bl-master-nodes.tf variable blocks
#===================================================

variable "master_count" {
  description = "This variable sets the number of master nodes to provision."
}

variable "master_name" {
  description = "This variable sets the master nodes' names."
}

variable "master_num_cpus" {
  description = "This variable sets the number of vCpus for each master node."
}

variable "master_memory" {
  description = "This variable sets the number of CPU memory in mbs for each master node."
}

#===================================================
# vSphere Data - bl-worker-nodes.tf variable blocks
#===================================================

variable "worker_count" {
  description = "This variable sets the number of worker nodes to provision."
}

variable "worker_name" {
  description = "This variable sets the worker nodes' names."
}

variable "worker_num_cpus" {
  description = "This variable sets the number of vCpus for each worker node."
}

variable "worker_memory" {
  description = "This variable sets the number of CPU memory in mbs for each worker node."
}

#============================================================================================================
# vSphere Data - bl-worker-nodes.tf and bl-master-nodes.tf common provisioner and connection variable blocks
#============================================================================================================

variable "node_ssh_key" {
  description = "Value of the public ssh key to use for node_user in the cloud-init configs."
}

variable "node_ssh_priv_keyfile" {
  description = "Location of the private ssh keyfile to use for ansible inventory generation"
}

variable "node_user" {
  description = "This variable uses the node user to establish initial ssh connection."
}

variable "cloud_init_template_image" {
  description = "This variable stores the cloud init template image name."
}

variable "ansible_inventory_dir" {
  description = "Location to drop the ansible inventory file into"
}
